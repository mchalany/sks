import handlers.base as base
import utils
import models


class UploadHandler(base.BaseHandler):

    def execute(self):
        data = {
            'nick': self.content['pseudonym'],
            'IK': self.content['IK'],
            'SPK': self.content['SPK']
        }
        self.users.update_user(data)
        print(f'IK and SPK keys for user {self.content["pseudonym"]} added!')


class RegisterHandler(base.BaseHandler):

    def execute(self):
        try:
            nick = self.content['pseudonym']
            cert = self.content['certificate']
            print(f'C --> S: (0) got cert ({len(cert)}) for nick {nick}')
        except Exception:
            print('Incorrect cert or nick from client!')
            return

        if nick not in self.users.online:
            user = models.User(nick, cert=cert)
            self.users.add_user(user)
            print(f'Accepted new user {nick}')
            triplet = utils.Triplet(0, 'ACCEPTED')
        else:
            print(f'Already existing or incorrect data, user: {nick}')
            triplet = utils.Triplet(0, 'FAILED')

        message = utils.create_message(1, [triplet, ])
        utils.send_message(
            message,
            'S --> C: (1) sent registration verdict',
            sock=self.request)
