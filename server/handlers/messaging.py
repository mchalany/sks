import handlers.base as base

import utils


class GetKeysHandler(base.BaseHandler):

    def execute(self):
        print('C --> S (11) received get keys request')
        user = self.users.get_user(self.content['sender'])
        recipient = self.users.get_user(self.content['recipient'])

        message = utils.create_message(12, [
            utils.Triplet(0, recipient.nick),
            utils.Triplet(7, recipient.IK),
            utils.Triplet(8, recipient.SPK)])

        utils.send_message(
            message,
            'S --> C (12) sending recipient keys to user',
            sock=user.get_socket())


class InitMessageHandler(base.BaseHandler):

    def execute(self):
        print('C --> S (13) received init message request')
        sender = self.users.get_users(self.content['sender'])
        recipient = self.users.get_users(self.content['recipient'])

        message = utils.create_message(13, [
            utils.Triplet(0, sender.nick),
            utils.Triplet(0, recipient.nick),
            utils.Triplet(7, self.content['IK']),
            utils.Triplet(11, self.content['EK']),
            utils.Triplet(6, self.content['AD_SK'])])

        utils.send_message(
            message,
            'S --> C (13) forwarding init message to recipient',
            sock=recipient.get_socket())


class EncryptHandler(base.BaseHandler):

    def execute(self):
        print('C --> S (14) received encrypted message')
        sender = self.users.get_user(self.content['sender'])
        recipient = self.users.get_user(self.content['recipient'])

        message = utils.create_message(14, [
            utils.Triplet(0, sender.nick),
            utils.Triplet(0, recipient.nick),
            utils.Triplet(0, self.content['message'])])

        utils.send_message(
            message,
            'S --> C (14) forwarding encrypted message',
            sock=recipient.get_socket())
