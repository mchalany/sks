import socket

import handlers.base as base

import utils


class DisconnectHandler(base.BaseHandler):

    def execute(self):
        print('C --> S: (16) received connection termination request')
        # todo: debug this! why what log off and disconnect collide not working
        # must be done one by one, so sleep on client side... but why?
        for user in self.users.registered:
            if user.nick in self.users.online and \
                    user.nick != self.content['pseudonym']:
                message = utils.create_message(3, [
                    utils.Triplet(0, ','.join(self.users.online))])
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                host, port = user.address.split(',')
                sock.connect((host, int(port)))
                utils.send_message(
                    message,
                    'S --> C (3) sending actualized online users list',
                    sock=sock)
                sock.close()


class LogOffHandler(base.BaseHandler):

    def execute(self):
        print('C --> S: (15) received log off request')
        user = self.users.get_user(self.content['pseudonym'])
        self.users.update_user({
            'nick': user.nick,
            'online': False
        })
