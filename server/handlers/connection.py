import time
import random
import string
import socket

from OpenSSL import crypto
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes

import handlers.base as base
import utils


class GetUsersHandler(base.BaseHandler):

    def execute(self):
        print('C --> S: (2) received get users request')
        users = ','.join([user.nick + ' - online' if user.online else user.nick
                         for user in self.users.registered])
        message = utils.create_message(3, [
            utils.Triplet(0, users)])
        utils.send_message(
            message,
            f'S --> C: (3) users pseudonyms sent: {users}',
            sock=self.request)


class ConnectHandler(base.BaseHandler):

    def execute(self):
        print('C --> S: (4) received connection request')
        address = self.content['address']
        challenge = ''.join(random.choices(
            string.ascii_letters + string.digits, k=16))
        message = utils.create_message(5, [
                utils.Triplet(3, challenge)])
        response = utils.send_message(
            message,
            'S --> C: (5) random challenge sent',
            'C --> S: (6) received signed challenge',
            self.request)

        nick = response['pseudonym']
        signature = response['signature']
        try:
            certificate = crypto.load_certificate(
                crypto.FILETYPE_ASN1, self.users.get_user(nick).cert)
            print(f'Certificate successfully loaded for {nick}')
        except Exception:
            print(f'Incorrect or missing certificate for user {nick}')
            return

        try:
            crypto.verify(
                certificate, signature, challenge, 'sha256')
            print('Signature verified!')
        except Exception:
            message = utils.create_message(7, [
                utils.Triplet(0, 'AUTHENTICATION_FAILED')])
            self.request.send(message)
            print(f'S --> C: (7) failed authentication ({nick})')
            return

        if nick not in self.users.online:
            self.users.update_user({
                'nick': nick,
                'log_time': time.time(),
                'address': address,
                'online': True,
                })
        else:
            print(f'{nick} is already online!')
            # todo: pridat moznost odhlasit usera zo servera
            # ak nejaka chyba a v skutocnosti nie je online
            return

        try:
            symmetric_key = get_random_bytes(16)
            public_key = crypto.dump_publickey(
                crypto.FILETYPE_PEM, certificate.get_pubkey())
            public_key = RSA.import_key(public_key)

            cipher_rsa = PKCS1_OAEP.new(public_key)
            cipher_text = cipher_rsa.encrypt(symmetric_key)
        except Exception as e:
            print(f'Error during user verifying! {e}')

        message = utils.create_message(7, [
            utils.Triplet(0, 'CONNECTED'),
            utils.Triplet(5, cipher_text)])
        response = utils.send_message(
            message,
            'S --> C: (7) connection status + fresh symmetric sent',
            'C --> S: (8) received signed challenge',
            self.request)

        aes = AES.new(symmetric_key, AES.MODE_ECB)
        if aes.decrypt(response['once']) != b'1000000000000000':
            message = utils.create_message(9, [
                utils.Triplet(0, 'INCORRECT_KEY')])
            self.request.send(message)
            print('S --> C: (9) incorrect key message sent')
        for nick in self.users.online:
            online = self.users.online.copy()
            online.remove(nick)
            online = [x + ' - online' for x in online]
            message = utils.create_message(3, [
                utils.Triplet(0, ','.join(online))])
            utils.send_message(
                message,
                'S --> C (3) sending actualized online users list',
                sock=self.users.get_user(nick).get_socket())
