from socketserver import ThreadingTCPServer
import threading
import pickle

import router
import settings


if __name__ == '__main__':
    with open('misc/pickles/users.pickle', 'wb') as file:
        pickle.dump({}, file)

    server = ThreadingTCPServer(settings.ADDRESS, router.RequestRouter)
    server.allow_reuse_thread = True
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()

    print('Welcome!')
    print('Server started. Listening for connections on port %i...\n'
          % settings.PORT)
