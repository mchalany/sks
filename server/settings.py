CLIENTS_PATH = 'clients/'

BUFFER_SIZE = 1024
HOST = '127.0.0.1'
PORT = 1234
ADDRESS = (HOST, PORT)

USERS_PICKLE = 'misc/pickles/users.pickle'
