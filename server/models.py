import time
import pickle
import socket

from settings import USERS_PICKLE


class Message:

    def __init__(self):
        pass


class User:

    def __init__(self, nick,
                 cert=None, IK=None, SPK=None,
                 address=None, last_log=None, online=False,
                 dtc=time.time()):
        self.nick = nick
        self.cert = cert
        self.IK = IK
        self.SPK = SPK
        self.address = address
        self.last_log = last_log
        self.online = online
        self.dtc = dtc

    def update_user(self, data):
        if 'nick' in data:
            self.nick = data['nick']
        if 'cert' in data:
            self.cert = data['cert']
        if 'IK' in data:
            self.IK = data['IK']
        if 'SPK' in data:
            self.SPK = data['SPK']
        if 'address' in data:
            self.address = data['address']
        if 'descriptor' in data:
            self.descriptor = data['descriptor']
        if 'last_log' in data:
            self.last_log = data['last_log']
        if 'online' in data:
            self.online = data['online']

    def get_socket(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host, port = self.address.split(',')
        # smthng wrong with this, connection keeps being refused
        # probably already closed...
        sock.connect((host, int(port)))
        # todo: close socket... where?
        return sock


class UserList:

    # todo: move this from primitive pickling to simple database
    @property
    def registered(self):
        registered = self.load()
        return registered.values()

    @property
    def online(self):
        online = []
        for user in self.registered:
            if user.online:
                online.append(user.nick)
        return online

    def get_user(self, nick):
        registered = self.load()
        return registered[nick]

    def add_user(self, user):
        registered = self.load()
        registered[user.nick] = user
        self.save(registered)

    def update_user(self, data):
        registered = self.load()
        registered[data['nick']].update_user(data)
        self.save(registered)

    def remove_user(self, nick):
        registered = self.load()
        registered.pop(nick)
        self.save(registered)

    def save(self, registered):
        with open(USERS_PICKLE, 'wb') as file:
            pickle.dump(registered, file)

    def load(self):
        try:
            with open(USERS_PICKLE, 'rb') as file:
                return pickle.load(file)
        except Exception:
            return {}

    def __str__(self):
        return ','.join([x for x in self.load()])
