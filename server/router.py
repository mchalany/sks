from socketserver import BaseRequestHandler

from handlers import (
    base,
    registration,
    connection,
    messaging,
    termination
)

import settings
import utils


class RequestRouter(BaseRequestHandler):

    def handle(self):
        try:
            recieved = self.request.recv(settings.BUFFER_SIZE)
            message_type, content = utils.parse_message(recieved)
            print(f'\nReceived request from: {self.request.getpeername()}')
        except Exception as e:
            print(f'Incorrect request came from client! {e}')
            return

        if message_type == 0:
            print('...routing request to RegisterHandler...')
            handler = registration.RegisterHandler(self.request, content)
        elif message_type == 2:
            print('...routing request to GetUsersHandler...')
            handler = connection.GetUsersHandler(self.request, content)
        elif message_type == 4:
            print('...routing request to ConnectHandler...')
            handler = connection.ConnectHandler(self.request, content)
        elif message_type == 10:
            print('...routing request to UploadHandler...')
            handler = registration.UploadHandler(self.request, content)
        elif message_type == 11:
            print('...routing request to GetKeysHandler...')
            handler = messaging.GetKeysHandler(self.request, content)
        elif message_type == 13:
            print('...routing request to InitMessageHandler...')
            handler = messaging.InitMessageHandler(self.request, content)
        elif message_type == 14:
            print('...routing request to EncryptHandler...')
            handler = messaging.EncryptHandler(self.request, content)
        elif message_type == 15:
            print('...routing request to LogOffHandler...')
            handler = termination.LogOffHandler(self.request, content)
        elif message_type == 16:
            print('...routing request to TerminateHandler...')
            handler = termination.DisconnectHandler(self.request, content)
        else:
            print('Wrong message type!')
            handler = base.BaseHandler(self.request, content)

        return handler.execute()
