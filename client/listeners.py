import tkinter as tk

import Crypto.Hash as hash
from Crypto.Protocol import KDF
import donna25519 as donna

import utils


class BaseListener:

    def __init__(self,  controller):
        self.ctrl = controller

    def execute(self):
        print('Not implemented yet!')


class GetUsersListener(BaseListener):

    def execute(self):
        print('S --> C (3) received registered users and marked online')
        self.ctrl.view.user_list.delete(0, tk.END)
        user_list = self.ctrl.content['pseudonyms']
        for x in user_list.split(','):
            if x != '' and x not in (
                    self.ctrl.login, self.ctrl.login + ' - online'):
                self.ctrl.view.user_list.insert(tk.END, x)


class GetKeysListener(BaseListener):

    def execute(self):
        # basically the second part of request keys action
        recipient = self.ctrl.content['recipient']
        print(f'S --> C (12) received keys for {recipient} from the server')
        IK = donna.PublicKey(self.ctrl.content['IK'])
        SPK = donna.PublicKey(self.ctrl.content['SPK'])

        S1 = self.ctrl.donna_keys['IK'].do_exchange(SPK)
        S2 = self.ctrl.donna_keys['EK'].do_exchange(IK)
        S3 = self.ctrl.donna_keys['EK'].do_exchange(SPK)
        SK = KDF.HKDF(
            S1 + S2 + S3, 32, None, hash.SHA256,
            num_keys=1, context=None)
        AD = self.ctrl.donna_keys['IK'].do_exchange(IK)
        AD_SK = utils.encrypt(AD, SK)

        # ukladam data osoby, s ktorou si chcem pisat ja
        self.ctrl.communication[recipient] = {
            'IK': IK,
            'SPK': SPK,
            'SK': SK,
            'AD': AD,
            'AD_SK': AD_SK,
            'messages': [f'Chatting with {recipient}'],
            'received': 0,
            'sent': 0
        }
        # a vytvaram specificke diffie-hellman kluce
        dh_private, dh_public = utils.gen_dh_pair()
        self.ctrl.communication[recipient].update({
            'dh_private': dh_private,
            'dh_public': dh_public
            })


class InitMessageListener(BaseListener):

    def execute(self):
        sender = self.ctrl.content['sender']
        print(f'S --> C (13) received init message from {sender}')
        IK = donna.PublicKey(self.ctrl.content['IK'])
        EK = donna.PublicKey(self.ctrl.content['EK'])
        S1 = self.ctrl.donna_keys['SPK'].do_exchange(IK)
        S2 = self.ctrl.donna_keys['IK'].do_exchange(EK)
        S3 = self.ctrl.donna_keys['SPK'].do_exchange(EK)
        SK = KDF.HKDF(
            S1 + S2 + S3, 32, None, hash.SHA256,
            num_keys=1, context=None)
        AD = self.ctrl.donna_keys['IK'].do_exchange(IK)
        AD_SK = utils.decrypt(AD, SK)

        # ukladam data osoby, ktora si so mnou chce pisat
        self.ctrl.communication[sender] = {
            'IK': IK,
            'EK': EK,
            'SK': SK,
            'AD': AD,
            'AD_SK': AD_SK,
            'messages': [f'Chatting with {sender}'],
            'received': 1,
            'sent': 0
        }
        # a vytvaram specificke diffie-hellman kluce
        dh_private, dh_public = utils.gen_dh_pair()
        self.ctrl.communication[sender].update({
            'dh_private': dh_private,
            'dh_public': dh_public
        })


class MessageListener(BaseListener):

    def execute(self):
        # ceknem ci public key je rovny tomu co naposledy
        # ak hej tak spravim prvy krat je urcite public key novy
        # cize robim DH exchange s mojim DH privatnym a mam nove
        sender = self.ctrl.content['sender']
        message = self.ctrl.content['message']
        print(f'S --> (14) received message from {sender}')

        # message je tvorene z (DH_PublicKey, pn, n) = header
        # header, {plaintext, (AD|| header)}_messsage_key

        # odkoho_len = to_int(received[12:16])
        # odkoho = received[16:16 + odkoho_len].decode('utf-8')
        # received = received[16 + odkoho_len:]
        # DH_pub_len = to_int(received[4:8])
        # DH_pub = received[8:8 + DH_pub_len]
        # encrypted = received[16 + DH_pub_len:]

        # self.dh_public_rec = donna.PublicKey(DH_pub)

        # # prijimam spravy podla toho ci som uz prijal
        # # a ci som uz odoslal
        # if self.odoslal == 0:
        #     self.root, chain_key0r = KDF.HKDF(
        #         self.secret_key, 32, None, hash.SHA256,
        #         num_keys=2, context=None)
        #     mkey0r, chain_key1r = KDF.HKDF(
        #         self.ad + chain_key0r, 32, None, hash.SHA256,
        #         num_keys=2, context=None)
        #     mkey1r, chain_key2r = KDF.HKDF(
        #         self.ad + chain_key1r, 32, None, hash.SHA256,
        #         num_keys=2, context=None)
        #     self.prijal = 1
        # elif self.odoslal == 1:
        #     SK = self.dh_private.do_exchange(self.dh_public_rec)
        #     self.root, chain_key0r = KDF.HKDF(
        #         self.root + SK, 32, None, hash.SHA256,
        #         num_keys=2, context=None)
        #     mkey0r, chain_key1r = KDF.HKDF(
        #         self.ad + chain_key0r, 32, None, hash.SHA256,
        #         num_keys=2, context=None)
        #     mkey1r, chain_key2r = KDF.HKDF(
        #         self.ad + chain_key1r, 32, None, hash.SHA256,
        #         num_keys=2, context=None)
        #     self.prijal = 1

        # plaintext = utils.decrypt(encrypted, mkey1r)

        if len(sender) > 20:
            od = sender[:20] + '...'
        else:
            od = sender
            for i in range(20-len(sender)-1):
                od += ' '
        # v skutocnosti nepridavat, az ked kliknem na usera sam
        # self.ctrl.view.msg_list.insert(tk.END, od + ':    ' + message)
        self.communication[sender].update({
            'message': [message]
        })
