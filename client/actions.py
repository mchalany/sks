import tkinter as tk
from tkinter import messagebox
import socket
import time

from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from OpenSSL import crypto
import Crypto.Hash as hash
from Crypto.Protocol import KDF

import settings
import utils


class BaseAction:

    def __init__(self,  controller):
        self.ctrl = controller

    def execute(self):
        print('Not implemented yet!')


class ConnectAction(BaseAction):

    def execute(self):
        """Challenge-response method for login
        """
        if not self.ctrl.login or not self.ctrl.certificate:
            messagebox.showerror('Error', 'Invalid credentials!')
            return

        sock = socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(settings.SERVER_ADDRESS)

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('127.0.0.1', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        address = ','.join([str(x) for x in s.getsockname()])

        message = utils.create_message(4, [
            utils.Triplet(0, 'CONNECTION_REQUEST'),
            utils.Triplet(0, address)])
        response = utils.send_message(
            message,
            'C --> S (4) sending connection request message',
            'S --> C: (5) receiving random challenge:',
            sock=sock)
        print(f'{response["challenge"]}')

        try:
            signed_challenge = crypto.sign(
                self.ctrl.private_key, response['challenge'], 'sha256')
        except Exception as e:
            print(f'Error during challenge signing! {e}')
            return

        message = utils.create_message(6, [
            utils.Triplet(0, self.ctrl.login),
            utils.Triplet(4, signed_challenge)])
        response = utils.send_message(
            message,
            'C --> S: (6) sending signed challenge with nick',
            'S --> C: (7) receiving symmetric key and result',
            sock=sock)
        print(f'result: {response["result"]}')

        if response['result'] == 'AUTHENTICATION_FAILED':
            messagebox.showerror('Error', 'Authentication failed!')
            return

        elif response['result'] == 'CONNECTED':
            pkey = self.ctrl.key_file
            rsa_key = RSA.importKey(pkey)
            cipher_rsa = PKCS1_OAEP.new(rsa_key)

            try:
                symmetric_key = cipher_rsa.decrypt(response['symmetric'])
                aes = AES.new(symmetric_key, AES.MODE_ECB)
                once = aes.encrypt(b'1000000000000000')
            except Exception as e:
                print(f'Error during once encrypting! {e}')
                return

            message = utils.create_message(8, [
                utils.Triplet(6, once)])
            utils.send_message(
                message,
                'C --> S: (8) sending once encrypted with symmetric key',
                sock=sock)

            try:
                response = utils.parse_message(
                    sock.recv(settings.BUFFER_SIZE))
                'S --> C: (9) received result of symmetric key test',
                print(response['title'])
                return
            except Exception:
                return s


class GetUsersAction(BaseAction):

    def execute(self):
        message = utils.create_message(2, [
            utils.Triplet(0, 'GET_REGISTERED_USERS'), ])
        response = utils.send_message(
            message,
            'C --> S (2) sending get users request message',
            'S --> C: (3) receiving list of users pseudonyms')

        self.ctrl.view.user_list.delete(0, tk.END)
        user_list = response['pseudonyms']
        for x in user_list.split(','):
            if x != '' and x not in (
                    self.ctrl.login, self.ctrl.login + ' - online'):
                self.ctrl.view.user_list.insert(tk.END, x)


class RegisterAction(BaseAction):

    def execute(self):

        if not self.ctrl.login:
            messagebox.showerror('Error', 'No login provided!')
            return

        IK_public = self.ctrl.donna_keys['IK'].get_public().public
        SPK_public = self.ctrl.donna_keys['SPK'].get_public().public

        message = utils.create_message(0, [
                utils.Triplet(0, 'REGISTRATION'),
                utils.Triplet(0, self.ctrl.login),
                utils.Triplet(2, self.ctrl.certificate),
                ])
        response = utils.send_message(
            message,
            'C --> S: (0) sending registration triplets',
            'S --> C: (1) receiving registration response:',)
        print(f'response: {response["decision"]}')

        if response['decision'] == 'FAILED':
            messagebox.showerror('Error', 'Registration unsuccessful!'
                                 'User is already registered...')
        elif response['decision'] == 'ACCEPTED':
            message = utils.create_message(10, [
                utils.Triplet(0, self.ctrl.login),
                utils.Triplet(7, IK_public),
                utils.Triplet(8, SPK_public),
                ])
            utils.send_message(
                message,
                'C --> S: (10) sending pub keys to server')
            messagebox.showinfo('Success', 'Registration successful! Please,'
                                ' proceed with login...')


class RequestKeysAction(BaseAction):

    def execute(self):
        self.ctrl.recipient = self.ctrl.recipient.split('-')[0][:-1]

        if not self.ctrl.recipient.find('- online'):
            # TODO: create database, so possible chat with offline users
            messagebox.showerror('Error', 'User not online!')
            return

        if self.ctrl.recipient in self.ctrl.communication:
            print('already chatting with recipient, showing messages')
            # already chatting with recipient, show locally stored messages
            for message in self.ctrl.communication[self.ctrl.recipient]['messages']:
                self.ctrl.view.msg_list.insert(tk.END, message)
            return
        else:
            self.ctrl.view.msg_list.delete(0, tk.END)

        message = utils.create_message(11, [
            utils.Triplet(0, 'GET_KEYS'),
            utils.Triplet(0, self.ctrl.login),
            utils.Triplet(0, self.ctrl.recipient)])
        utils.send_message(
            message,
            'C --> S (11) sending get keys request to server')


class SendAction(BaseAction):

    def execute(self):
        # po kliknuti na jeho meno sa posiela request na kluce ak ich
        # este nemam, takze v komunikacii tieto data musia byt ulozene
        rec_num = self.ctrl.communication[self.ctrl.recipient]['received']
        sent_num = self.ctrl.communication[self.ctrl.recipient]['sent']
        AD_SK = self.ctrl.communication[self.ctrl.recipient]['AD_SK']

        print(f'C1 --> C2 (13) init message pre {self.ctrl.recipient}')

        if not sent_num and not rec_num:
            # ak sme si este nevymenili ziadne spravy, tak len init message
            utils.create_message(13, [
                utils.Triplet(0, self.ctrl.login),
                utils.Triplet(0, self.ctrl.recipient),
                utils.Triplet(7, self.ctrl.donna_keys['IK_pub']),
                utils.Triplet(8, self.ctrl.donna_keys['SPK_pub']),
                utils.Triplet(6, AD_SK)])

        # probably sleep here???

        if not self.ctrl.recipient:
            messagebox.showwarning('Warning!', 'Choose recipient!')
            return

        message = self.ctrl.view.my_msg.get()
        if not message:
            return

        # # prvy krat posielam
        # if not rec_num:
        #     # self.root = self.secret_key# root0
        #     self.dh_private, self.dh_public = utils.gen_dh_pair()
        #     self.root, chain_key0s = KDF.HKDF(
        #         self.secret_key, 32, None, hash.SHA256,
        #         num_keys=2, context=None)  # root1
        #     mkey0s, chain_key1s = KDF.HKDF(
        #         self.ad + chain_key0s, 32, None, hash.SHA256,
        #         num_keys=2, context=None)
        #     mkey1s, chain_key2s = KDF.HKDF(
        #         self.ad + chain_key1s, 32, None, hash.SHA256,
        #         num_keys=2, context=None)
        # # dalsi krat posielam, ked uz som prijal
        # # cize musim spravit diffie hellmana kvoli dh ratchet
        # elif self.prijal == 1:
        #     self.dh_private, self.dh_public = utils.gen_dh_pair()
        #     SK1 = self.dh_private.do_exchange(self.dh_public_rec)
        #     self.root, chain_key0s = KDF.HKDF(
        #         self.root + SK1, 32, None, hash.SHA256,
        #         num_keys=2, context=None)  # root 2
        #     mkey0s, chain_key1s = KDF.HKDF(
        #         self.ad + chain_key0s, 32, None, hash.SHA256,
        #         num_keys=2, context=None)
        #     mkey1s, chain_key2s = KDF.HKDF(
        #         self.ad + chain_key1s, 32, None, hash.SHA256,
        #         num_keys=2, context=None)

        # self.odoslal = 1

        # # formatovanie pred odoslanim
        # # este plus samotny key kedze je v bytes
        # dh_tlv = '0012' + to_4b(len(self.dh_public))
        # bdh_tlv = bytes(dh_tlv, 'utf-8')
        # assoc_data = self.ad + self.dh_public
        # enc_data = encrypt(bytes(plaintext, 'utf-8'), mkey1s, assoc_data)
        # # este plus samotne data kedze su v bytes
        # enc_string = '0005' + to_4b(len(enc_data))

        # komu = to_triplet('0000', self.adresat)
        # dlzka = to_4b(len(komu) + len(dh_tlv) + len(self.dh_public)
        #               + len(enc_string) + len(enc_data))
        # sprava = bytes('0014' + dlzka + komu + dh_tlv, 'utf-8') \
        #     + self.dh_public + bytes(enc_string, 'utf-8') + enc_data

        to_send = utils.create_message(14, [
            utils.Triplet(0, self.ctrl.login),
            utils.Triplet(0, self.ctrl.recipient),
            utils.Triplet(0, message)])
        utils.send_message(
            to_send,
            f'C1 --> C2 (14) akoze message pre {self.ctrl.recipient}')

        ja = 'Ja'
        for i in range(16-len(ja)-1):
            ja += ' '
        ja += ':  '
        self.ctrl.view.msg_list.insert(tk.END, ja + message)
        # self.session.send(sprava)
        self.ctrl.view.my_msg.set('')


class TerminateAction(BaseAction):

    def execute(self):
        # log off... wait... terminate (eliminate that wait part)
        message = utils.create_message(15, [
            utils.Triplet(0, 'LOG_OFF'),
            utils.Triplet(0, self.ctrl.login)]
        )
        utils.send_message(
            message,
            'C --> S: (15) sending log off message')
        # TODO: sleep because if not one by one doesnt work... fix!
        time.sleep(1)
        message = utils.create_message(16, [
                utils.Triplet(0, 'CONNECTION_TERMINATED'),
                utils.Triplet(0, self.ctrl.login)])
        utils.send_message(
            message,
            'C --> S: (16) sending connection termination message')
