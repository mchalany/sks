import listeners
import utils
import settings


class Router:

    def __init__(self, ctrl):
        self.ctrl = ctrl

    def route(self):
        try:
            self.ctrl.socket.listen()
            conn, addr = self.ctrl.socket.accept()
            recv = conn.recv(settings.BUFFER_SIZE)
        except Exception:
            return

        message_type, self.ctrl.content = utils.parse_message(recv)

        if message_type == 3:
            listeners.GetUsersListener(self.ctrl).execute()
        elif message_type == 12:
            listeners.GetKeysListener(self.ctrl).execute()
        elif message_type == 13:
            # prijimam prvotny message s klucmi
            listeners.InitMessageListener(self.ctrl).execute()
        elif message_type == 14:
            # prijatie kazdej spravy okrem prvej
            listeners.MessageListener(self.ctrl).execute()
        elif message_type == 16:
            # tu prijmem info ze recipient ukoncil komunikaciu
            # zmazem ho zo self.commnication...
            print('msg type 16')
        else:
            print('other msg type')
            return
