import tkinter as tk
import os

from controllers import LoginController


if __name__ == '__main__':
    for file in os.listdir('misc/certificates/'):
        os.remove(os.path.join('misc/certificates/', file))
    for file in os.listdir('misc/keys/'):
        os.remove(os.path.join('misc/keys/', file))

    root = tk.Tk()
    root.withdraw()

    LoginController(root)
    root.mainloop()
