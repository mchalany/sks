import socket
import threading
import sys

from OpenSSL import crypto
from asn1crypto import x509
import donna25519 as donna

import settings

from views import LoginView, ChatView
import router
import actions


class BaseController:

    def __init__(self, root):
        self.root = root


class LoginController(BaseController):

    def __init__(self, root):
        super().__init__(root)
        self.view = LoginView(self, root)

    @property
    def socket(self):
        if not getattr(self, '_socket'):
            self._socket = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            self._socket.connect(settings.SERVER_ADDRESS)
        return self._socket

    @property
    def login(self):
        return self.view.login_entry.get()

    @property
    def donna_keys(self):
        if not hasattr(self, '_donna_keys'):
            keys = ('IK', 'SPK', 'EK')
            self._donna_keys = {}
            for key in keys:
                self._donna_keys[key] = donna.PrivateKey()
            self._donna_keys['IK_pub'] = \
                self.donna_keys['IK'].get_public().public
            self._donna_keys['SPK_pub'] = \
                self.donna_keys['SPK'].get_public().public
        return self._donna_keys

    @property
    def key_path(self):
        return 'misc/keys/' + self.login + '.pem'

    @property
    def cert_path(self):
        return 'misc/certificates/' + self.login + '.der'

    @property
    def key_file(self):
        # TODO: key object to bytes, instead of save and load
        with open(self.key_path, 'rb') as file:
            return file.read()

    @property
    def cert_file(self):
        # TODO: cert object to bytes, instead of save and load
        with open(self.cert_path, 'rb') as file:
            return file.read()

    @property
    def private_key(self):
        if not hasattr(self, '_private_key'):
            self._private_key = crypto.PKey()
            self._private_key.generate_key(crypto.TYPE_RSA, 2048)
            with open(self.key_path, 'wt') as file:
                file.write(crypto.dump_privatekey(
                        crypto.FILETYPE_PEM,
                        self._private_key).decode('utf-8'))
        return self._private_key

    @property
    def certificate(self):
        if not hasattr(self, '_certificate'):
            self._certificate = self.generate_certificate(
                self.login, self.private_key)
            with open(self.cert_path, 'wb') as file:
                file.write(self._certificate.dump())
        # TODO: self.certificate object to bytes, not load from file
        return self.cert_file

    @staticmethod
    def generate_certificate(login, private_key):
        certificate = crypto.X509()
        certificate.get_subject().C = 'SK'
        certificate.get_subject().ST = 'Bratislava'
        certificate.get_subject().L = 'Bernolakova'
        certificate.get_subject().O = 'FEI'
        certificate.get_subject().OU = 'STU'
        certificate.get_subject().CN = login
        certificate.set_serial_number(1000)
        certificate.gmtime_adj_notBefore(0)
        certificate.gmtime_adj_notAfter(10 * 365 * 24 * 60 * 60)
        certificate.set_issuer(certificate.get_subject())
        certificate.set_pubkey(private_key)
        certificate.sign(private_key, 'sha256')
        certificate = x509.Certificate.load(
            crypto.dump_certificate(
                crypto.FILETYPE_ASN1, certificate))
        return certificate

    def user_action(self, event):
        if event == 'register':
            actions.RegisterAction(self).execute()
        elif event == 'connect':
            socket = actions.ConnectAction(self).execute()
            if socket:
                self.view.withdraw()
                ChatController(self.root,
                               socket,
                               self.login,
                               self.donna_keys)


class ChatController(BaseController):

    def __init__(self, root, socket, login, donna_keys):
        super().__init__(root)
        self.login = login
        self.socket = socket
        self.donna_keys = donna_keys
        self.view = ChatView(self, root)

        # tato premenna drzi v sebe spravy od inych uzivatelov :/
        self.communication = {}

        # self.dh_public_rec = ''
        # self.root = ''

        self.recipient = None

        actions.GetUsersAction(self).execute()
        threading.Thread(target=router.Router(self).route).start()

    def user_action(self, event, data=None):
        if event == 'terminate':
            actions.TerminateAction(self).execute()
            self.view.quit()
            self.view.destroy()
            sys.exit()
        elif event == 'disconnect':
            actions.TerminateAction(self).execute()
        elif event == 'request_keys':
            self.recipient = data
            actions.RequestKeysAction(self).execute()
        elif event == 'send':
            actions.SendAction(self).execute()
