import tkinter as tk
import sys


class LoginView(tk.Toplevel):

    def __init__(self, controller, root):
        tk.Toplevel.__init__(self, root)
        self.ctrl = controller

        self.title('Registration/Login')
        self.geometry('250x180')

        self.protocol("WM_DELETE_WINDOW", self.exit)
        # self.frame = tk.Frame(self)
        # self.frame.config(bg='navy')

        self.login_label = tk.Label(
            self, text='Provide your pseudonym:')
        self.login_label.config(fg='navy')
        self.login_label.place(x=20, y=0, width=210, height=20)
        self.login_name = tk.StringVar()
        self.login_entry = tk.Entry(self, textvariable=self.login_name)
        self.login_entry.config(width=100)
        self.login_entry.place(x=20, y=20, width=210, height=25)

        self.reg_button = tk.Button(
            self, text='Sign up',
            command=lambda: self.ctrl.user_action('register'))
        self.reg_button.config(bg='navy', fg='white')
        self.reg_button.place(x=40, y=120, width=80, height=40)

        self.connect_button = tk.Button(
            self, text='Login',
            command=lambda: self.ctrl.user_action('connect'))
        self.connect_button.place(x=130, y=120, width=80, height=40)
        self.connect_button.config(bg='green', fg='white')

    def exit(self):
        self.quit()
        self.destroy()
        sys.exit(0)


class ChatView(tk.Toplevel):

    def __init__(self, controller, root):
        tk.Toplevel.__init__(self, root)
        self.ctrl = controller

        self.resizable(False, False)
        self.geometry('800x460')
        self.title(self.ctrl.login)
        self.protocol("WM_DELETE_WINDOW",
                      lambda: self.ctrl.user_action('terminate'))

        # self.frame = tk.Frame(self)
        # self.frame.place(x=0, y=0, width=600, height=400)

        self.scrollbar = tk.Scrollbar(self)
        self.scrollbar.place(x=580, y=0, width=20, height=400)

        self.my_msg = tk.StringVar()
        self.my_users = tk.StringVar()

        self.msg_list = tk.Listbox(
            self, yscrollcommand=self.scrollbar.set)
        self.msg_list.place(x=0, y=0, width=580, height=400)

        self.user_list = tk.Listbox(self)
        self.user_list.place(x=600, y=0, width=200, height=360)
        self.user_list.bind('<<ListboxSelect>>', self.click_on_user)

        self.entry_field = tk.Entry(self, textvariable=self.my_msg)
        self.entry_field.place(x=110, y=410, width=690, height=40)
        # toto by neslo tak ako obdobne nesisel user_list bind, mozno zmenit
        # self.bind("<Return>", lambda: self.ctrl.user_action('send'))

        self.send_button = tk.Button(
            self, text="Send", command=lambda: self.ctrl.user_action('send'))
        self.send_button.place(x=0, y=410, width=100, height=40)

        self.logoff_button = tk.Button(
            self, text='Log off',
            command=lambda: self.ctrl.user_action('terminate'))
        self.logoff_button.place(x=600, y=360, width=100, height=40)

    def click_on_user(self, event):
        selection = event.widget.curselection()
        if selection:
            recipient = event.widget.get(selection[0])
            self.ctrl.user_action('request_keys', recipient)
