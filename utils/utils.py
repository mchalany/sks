from typing import Tuple
import socket

from Crypto.Cipher import AES
import donna25519 as donna

import settings


RESPONSE_VAR = {
    '0': ['title', 'pseudonym', 'certificate'],
    '1': ['decision'],
    '2': ['title'],
    '3': ['pseudonyms'],
    '4': ['title', 'address'],
    '5': ['challenge'],
    '6': ['pseudonym', 'signature'],
    '7': ['result', 'symmetric'],
    '8': ['once'],
    '9': ['title'],
    '10': ['pseudonym', 'IK', 'SPK'],
    '11': ['title', 'sender', 'recipient'],
    '12': ['recipient', 'IK', 'SPK'],
    '13': ['sender', 'recipient', 'IK', 'EK', 'AD_SK'],
    '14': ['sender', 'recipient', 'message'],
    '15': ['title', 'pseudonym'],
    '16': ['title', 'pseudonym']
}


def gen_dh_pair():
    private = donna.PrivateKey()
    public = private.get_public().public
    return private, public


def encrypt(plaintext, key, assoc_data=None):
    aes = AES.new(key, AES.MODE_CCM)
    msg = aes.nonce + aes.encrypt(plaintext) + aes.digest()
    return msg


def decrypt(msg, key):
    nonce = msg[0:11]
    ciphertext = msg[11:-16]
    aes = AES.new(key, AES.MODE_CCM, nonce)
    plaintext = aes.decrypt(ciphertext)
    return plaintext


def send_message(message, info, resp=None, sock=None):
    # TODO: maybe add "to" argument with user.address data
    # for sending to specific user
    # hold on... is this working correctly? no need to bind??
    ssocket = sock or socket.socket(
        socket.AF_INET, socket.SOCK_STREAM)
    try:
        ssocket.connect(settings.SERVER_ADDRESS)
    except Exception:
        pass

    try:
        print(info)
        ssocket.send(message)
    except Exception as e:
        print(f'Unable to send message! {e}')

    if resp:
        print(resp)
        try:
            _, response = parse_message(
                ssocket.recv(settings.BUFFER_SIZE))
        except Exception as e:
            print(f'Error in parsing message! {e}')
    else:
        response = None

    if not sock:
        ssocket.close()

    return response


def parse_message(message: bytes) -> Tuple[int, dict]:
    msg_type = message[:4].decode('utf-8')
    msg_type = msg_type.lstrip('0') if msg_type != '0000' else '0'
    content = message[8:]

    response = {}
    for variable in RESPONSE_VAR[msg_type]:
        try:
            tag = int(content[:4].decode('utf-8').lstrip('0'))
        except Exception:
            tag = 0
        length = int(content[4:8].decode('utf-8').lstrip('0'))
        value = content[8:length]
        triplet = Triplet(tag, value)

        response[variable] = triplet.value
        content = content[length:]
    return int(msg_type), response


def create_message(type_id: int, triplets: list) -> bytes:
    type_id = f"{type_id:0>4}"
    content = b''
    for triplet in triplets:
        content += triplet.bytes_form
    length = 4 + 4 + len(content)
    return bytes(type_id + f"{length:0>4}", 'utf-8') + content


class Triplet():

    def __init__(self, tag, value):
        self.tag = tag
        self._value = value

    @property
    def length(self):
        length = 4 + 4 + len(self.value)
        return f"{length:0>4}"

    @property
    def value(self):
        try:
            self._value = self._value.decode('utf-8')
        finally:
            return self._value

    @property
    def bytes_form(self):
        value = (
            bytes(f"{self.tag:0>4}", 'utf-8') +
            bytes(self.length, 'utf-8')
        )
        if not isinstance(self.value, bytes):
            value += bytes(self.value, 'utf-8')
        else:
            value += self.value
        return value


class Message:

    def __init__(self,
                 msg_type: int,
                 length: int,
                 triplets: list):
        self.msg_type = msg_type
        self.length = length
        self.triplets = triplets
