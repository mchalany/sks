# TCP instant messaging chat app secured with DoubleRatchet protocol

Started as project for school course Network encryption, this project contains server and client side of TCP instant messaging chat service secured with DoubleRatchet protocol.

## USAGE

Create venv and install packages from requirements.txt file  
Start server and at least two clients

```bash
python server/app.py
python client/app.py
```
